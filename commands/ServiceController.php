<?php

namespace app\commands;

use yii;
use yii\console\Controller;

/**
 * Class ServiceController - This service performs 3 actions:
 *   1. Get messages from Amazon SQS
 *   2. Process and save locally result image
 *   3. Uploads result image to Amazon S3
 * @package app\commands
 * @author Stanislav Hudkov <stanislav.hudkov@iqria.com>
 */
class ServiceController extends Controller
{
    public function actionIndex()
    {
        date_default_timezone_set('Europe/Kiev');

        while (true) {
            $messages = Yii::$app->queueManager->read();

            if (!$messages || empty($messages['Messages'])) {
                continue;
            }

            $messagesArray = $messages['Messages'];

            foreach ($messagesArray as $msg) {
                $msgBody = json_decode($msg['Body'], true);
                $sourcePath = Yii::$app->params['paths']['uploads'] . $msgBody['file'];
                $croppedFilePath = Yii::$app->params['paths']['converted'];

                if (is_file($sourcePath)) {
                    $resize = !empty($msgBody['resize']) ? explode(';', $msgBody['resize']) : [];
                    if ($resize) {
                        $croppedName = Yii::$app->imageConverter->cropHexagon($sourcePath, $msgBody['cropSize'], $msgBody['rotate']);
                        Yii::$app->imageConverter->resize($croppedFilePath . $croppedName, $resize[0], $resize[1]);
                    } else {
                        $croppedName = Yii::$app->imageConverter
                            ->cropHexagon($sourcePath, $msgBody['cropSize'], $msgBody['rotate']);
                    }

                    $croppedFilePath .= $croppedName;

                    if (is_file($croppedFilePath)) {
                        $fileContents = file_get_contents($croppedFilePath);
                        if (Yii::$app->resourceManager->save($fileContents, $croppedName)) {
                            Yii::$app->queueManager->dequeue($msg);
                            unlink($croppedFilePath);
                            $logDate = date('Y-m-d H:i:s');
                            echo "[{$logDate}] " . Yii::$app->resourceManager->getUrl($croppedName) . "\n";
                        }
                    }
                }
            }
        }
    }
}