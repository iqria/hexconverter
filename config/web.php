<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'name' => 'Hexer',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            'cookieValidationKey' => 'o7GYlS-ZNEdeT4tkLUbLPzHYV8IKf1ka',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => '/site/index',
                'view' => '/site/view-file',
                'view/<fileId:\w+>' => '/site/view-file'
            ]
        ],
        'imageConverter' => [
            'class' => 'app\components\ImageConverter',
            'destinationDirectory' => $params['paths']['converted']
        ],
        'resourceManager' => [
            'class' => 'app\components\ResourceManager',
            'key' => $params['amazon']['key'],
            'secret' => $params['amazon']['secret'],
            'bucket' => $params['amazon']['bucket']
        ],
        'queueManager' => [
            'class' => 'app\components\QueueManager',
            'awsKey' => $params['amazon']['key'],
            'awsSecret' => $params['amazon']['secret'],
            'awsRegion' => $params['amazon']['region'],
            'queueUrl' => $params['amazon']['queueUrl']
        ],
    ],
    'params' => $params,
];

return $config;
