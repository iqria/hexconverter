<?php

$params = require(__DIR__ . '/params.php');

return [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'gii'],
    'controllerNamespace' => 'app\commands',
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'imageConverter' => [
            'class' => 'app\components\ImageConverter',
            'destinationDirectory' => $params['paths']['converted']
        ],
        'resourceManager' => [
            'class' => 'app\components\ResourceManager',
            'key' => $params['amazon']['key'],
            'secret' => $params['amazon']['secret'],
            'bucket' => $params['amazon']['bucket']
        ],
        'queueManager' => [
            'class' => 'app\components\QueueManager',
            'awsKey' => $params['amazon']['key'],
            'awsSecret' => $params['amazon']['secret'],
            'awsRegion' => $params['amazon']['region'],
            'queueUrl' => $params['amazon']['queueUrl']
        ]
    ],
    'params' => $params,
];
