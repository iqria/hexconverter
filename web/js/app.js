$(function(){
	App.cropper = new App.Cropper({
		el: $('.wrapper'),
		options: {
			aspectRatio: 1.165,
			minWidth: 100,
			zoomable: false
		}
	})
})