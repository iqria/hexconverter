/**
 * @namespace App
 */
App = window.App || {};

/**
 * @class Cropper
 * @param {Object} config Object of configuration for this app.
 */
App.Cropper = function(config) {
	$.extend(this, config);

	this.init();
}

App.Cropper.prototype = {
	
	/**
	 * @cfg {jQuery} el Root element for this app.
	 */
	el: null,
	
	/**
	 * @cfg {Object} options Configuration for cropper plugin.
	 */
	options: null,
	
	/**
	 * @property {jQuery} img Image for init cropper plugin.
	 */
	img: null,
	
	/**
	 * @property {Object} file Image which uploaded locally.
	 */
	file: null,
	
	/**
	 * @property {Boolean} rotated If true -> application knows that hexagon is rotated.
	 */
	rotated: false,
	
	/**
	 * @property {Object} cropperData Data of position and size of hexagon.
	 */
	cropperData: null,
	
	/**
	 * @property {Number} timer Timer of calls to server for check image.
	 */
	timer: null,
	
	/**
	 * @property {Number} checkCounter Counter of calls to server for check image.
	 */
	checkCounter: null,
	
	/**
	 * @property {Boolean} empty If true -> application in initial state.
	 */
	empty: true,
	
	/**
	 * Initialization app. 
	 */
	init: function() {
		this.img = $('> img', this.el);
		
		this.initEvents();
		this.initPlugins();
	},
	
	/**
	 * Bind events.
	 */
	initEvents: function() {
		$('.file').on('change', this.onImageChange.bind(this));
		$('.reset').on('click', this.reset.bind(this));
		$('.crop').on('click', this.onCropClick.bind(this));
		$('.rotate').on('click', this.onRotateClick.bind(this));
	},
	
	/**
	 * Initialization of plugins.
	 */
	initPlugins: function() {
		this.img.cropper(this.options);
	},
	
	/**
	 * Calls when image is uploaded locally.
	 * @param   {Object}  e Event object.
	 */
	onImageChange: function(e) {
		var fileReader = new FileReader(),
        	files = e.target.files,
         	file = files[0],
			limitSize = 1000000,
			imageReg = /^image\/\w+$/;

		if (!files.length) {
			return;
		}
		
		this.file = file;

		if (imageReg.test(file.type)) {
			if(file.size > limitSize) {
				swal("Is too big image", "Please choose smaller image", "error");
				return false;
			}
			
			this.activateBtns();
			fileReader.readAsDataURL(file);
			fileReader.onload = this.onImageLoad.bind(this);
		}
		else {
			swal("Only image!", "Please choose an image file.", "error");
		}
	},
	
	/**
	 * Calls after user click on "Hex it" button.
	 */
	onCropClick: function() {
		if(this.file) {
			this.crop();
		}
	},
	
	/**
	 * Calls after user click on "Rotate" button.
	 */
	onRotateClick: function() {
		if(this.file) {
			this.rotate();
		}
	},
	
	/**
	 * Gets form data, append additional fields and sends it on server.
	 */
	crop: function() {
		var form = $('.file-form').get(0),
			formData = new FormData(form),
			cropData = this.getData();
		
		formData.append('cropSize', cropData.cropSize);
		formData.append('rotate', cropData.rotate);
		
		this.sendData(formData);
	},
	
	/**
	 * Rotate hexagon.
	 */
	rotate: function() {
		var hex = $('.hex', this.el),
			imgPath = null,
			img = this.img;
		
		this.cropperData = this.getCropperData();
		
		if(this.rotaded) {
			img.cropper('setAspectRatio', this.options.aspectRatio);
			img.cropper('setData', $.extend(this.cropperData, {
				width: this.cropperData.height * 1.165,
				x: this.cropperData.x - (this.cropperData.height - this.cropperData.width)
			}));

			imgPath = 'img/hex.png';
			this.rotaded = false;
		}
		else {
			img.cropper('setAspectRatio', 0.835);
			img.cropper('setData', $.extend(this.cropperData, {
				width: this.cropperData.height * 0.835,
				x: this.cropperData.x + (this.cropperData.width - this.cropperData.height)
			}));
			
			imgPath = 'img/hex2.png';
			this.rotaded = true;
		}
		
		hex.attr('src', imgPath);
	},
	
	/**
	 * Sends hexagon data to server.
	 * @param {Object} data Object contain form data(file) and additional fields.
	 */
	sendData: function(data) {
		$.ajax({
			url: '/upload',
			type: 'POST',
			mimeType:"multipart/form-data",
			data: data,
			contentType: false,
			cache: false,
			processData:false,
			success: this.onSendSuccess.bind(this),
			error: this.onSendError.bind(this)
		});
		
		this.showLoader('body');
	},
	
	/**
	 * Shows message and launches timer which makes check for image on server.
	 * @callback sendData
	 * @param {Object} response Response contain fileId -> identificator of image.
	 */
	onSendSuccess: function(response) {
		var response = JSON.parse(response);
		
		this.timer = setInterval(this.checkServerImg.bind(this), 1000, response.fileId);
		swal("Image uploaded", "Please wait, your image is processing...");
	},
	
	/**
	 * Shows error message.
	 * @callback sendData
	 */
	onSendError: function() {
		this.hideLoader();
		swal("Something wrong", "Image was not uploaded", "error");
	},
	
	/**
	 * Makes check for existing image on server and increases checkCounter.
	 * @param {Number} id Identificator of image.
	 */
	checkServerImg: function(id) {
		$.ajax({
			url: '/view/' + id,
			contentType: 'application/json',
			type: 'GET',
			success: this.onCheckSuccess.bind(this),
			error: this.onCheckError.bind(this)
		});
		
		this.checkCounter++;
	},
	
	/**
	 * Clears check-timer. Hides loader and shows result.
	 * @callback checkServerImg
	 * @param {Object} response Response contain fileUrl - url of image.
	 */
	onCheckSuccess: function(response) {
		clearInterval(this.timer);
		this.checkCounter = 0;
		this.hideLoader();
		this.showResult(response.fileUrl);
	},
	
	/**
	 * Shows message if checkServerImg launched more than 10 times.
	 * @callback checkServerImg
	 */
	onCheckError: function() {
		if(this.checkCounter == 10) {
			swal("Please wait", "Processing take more time...");
		}
	},
	
	/**
	 * Returns plugin data.
	 * @returns {Onject}
	 */
	getCropperData: function() {
		return this.img.cropper('getData', true);
	},
	
	/**
	 * Gets plugin data, adds "rotate" and "cropSize" fields and returns it.
	 * @returns {Object} Crop data.
	 */
	getData: function() {
		var cropperData = this.getCropperData();
		
		return {
			rotate: this.rotaded ? 90 : 0,
			cropSize: cropperData.x + ';' + cropperData.y + ';' + (cropperData.x + cropperData.width) + ';' + (cropperData.y + cropperData.height)
		};
	},
	
	/**
	 * Calls when image is uploaded locally. Hides drop zone and shows new image in plugin.
	 * @param {Object} e Event data.
	 */
	onImageLoad: function(e) {
		this.empty = false;
		this.hideDropZone();
		this.img.cropper(this.options).cropper("replace", e.target.result);
	},
	
	/**
	 * Hides drop zone.
	 */
	hideDropZone: function() {
		$('.drop-zone').hide();
		this.el.addClass('shown');
	},
	
	/**
	 * Show drop zone.
	 */
	showDropZone: function() {
		$('.drop-zone').show();
		this.el.removeClass('shown');
	},
	
	/**
	 * Makes app to initial state.
	 */
	reset: function() {
		if (this.empty)
			return false;
		
		this.showDropZone();
		this.img.attr('src', '');
		this.img.cropper('reset');
		this.disableBtns();
		this.hideResult();
		$('.drop-zone form').get(0).reset();
		this.rotaded = false;
		this.empty = true;
		$('.hex', this.el).attr('src', 'img/hex.png');
		this.img.cropper('setAspectRatio', this.options.aspectRatio);
	},
	
	/**
	 * Activates control buttons.
	 */
	activateBtns: function() {
		$('.crop').removeAttr('disabled');
		$('.rotate').removeAttr('disabled');
	},
	
	/**
	 * Deactivates buttons.
	 */
	disableBtns: function() {
		$('.crop').attr('disabled', 'disabled');
		$('.rotate').attr('disabled', 'disabled');
	},
	
	/**
	 * Shows hexagon image, url and button for download image.
	 * @param {String} url Url of image.
	 */
	showResult: function(url) {
		var resultEl = $('.result');
		
		$('img', resultEl).attr('src', url);
		$('a', resultEl).attr('href', url);
		$('pre', resultEl).html(url);
		
		resultEl.show();
	},
	
	/**
	 * Hides block with result.
	 */
	hideResult: function() {
		$('.result').hide();
	},
	
	/**
	 * Show loader.
	 * @param {String} selector Selector which will be display loader.
	 */
	showLoader: function(selector) {
		var selector = $(selector) || $('body'),
			loaderTpl = [
			'<div class="loader-container">',
				'<div class="socket">',
					'<div class="gel center-gel">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c1 r1">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c2 r1">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c3 r1">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c4 r1">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c5 r1">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c6 r1">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c7 r2">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c8 r2">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c9 r2">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c10 r2">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c11 r2">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c12 r2">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c13 r2">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c14 r2">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c15 r2">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c16 r2">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c17 r2">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c18 r2">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c19 r3">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c20 r3">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c21 r3">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c22 r3">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c23 r3">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c24 r3">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c25 r3">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c26 r3">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c28 r3">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c29 r3">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c30 r3">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c31 r3">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c32 r3">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c33 r3">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c34 r3">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c35 r3">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c36 r3">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
					'<div class="gel c37 r3">',
						'<div class="hex-brick h1"></div>',
						'<div class="hex-brick h2"></div>',
						'<div class="hex-brick h3"></div>',
					'</div>',
				'</div>',
			'</div>'].join('');
		
		selector.append(loaderTpl);
	},
	
	/**
	 * Hides loader.
	 */
	hideLoader: function() {
		$('.loader-container').remove();
	}
	
}