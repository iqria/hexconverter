<?php

namespace app\components;

use yii;
use dosamigos\resourcemanager\AmazonS3ResourceManager;
use Aws\S3\Enum\CannedAcl;
use yii\helpers\ArrayHelper;

class ResourceManager extends AmazonS3ResourceManager
{
    public function save($fileContents, $name, $options = [])
    {
        $options = ArrayHelper::merge([
            'Bucket' => $this->bucket,
            'Key' => $name,
            'Body' => $fileContents,
            'ACL' => CannedAcl::PUBLIC_READ
        ], $options);

        return $this->getClient()->putObject($options);
    }
}