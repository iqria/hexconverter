<?php

namespace app\components;

use yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

/**
 * Class ImageConverter - the main feature of this class is that it can crop your images in the shape of hexagons
 * @package app\components
 * @author Stanislav Hudkov <stanislav.hudkov@iqria.com>
 */
class ImageConverter extends Component
{
    /**
     * @var string $destinationDirectory destination images path
     */
    public $destinationDirectory;

    public function init()
    {
        if (!extension_loaded('imagick')) {
            throw new InvalidConfigException('Imagick extension is not installed.', 500);
        }

        if (!$this->destinationDirectory) {
            throw new InvalidConfigException('Destination directory must be set.', 500);
        }
    }

    /**
     * Check the user selected square image size
     * @param int $width
     * @param int $height
     * @return $this
     * @throws \yii\base\InvalidConfigException
     */
    public function validateAspectRatio($width, $height)
    {
        if ($width !== $height) {
            throw new InvalidConfigException('Width and height must be equal.');
        }

        return $this;
    }

    /**
     * Fix image orientation based on EXIF data
     * @param string $filePath
     */
    public function fixOrientation($filePath)
    {
        $image = new \Imagick($filePath);

        $orientation = $image->getImageProperty('exif:Orientation');

        if (!empty($orientation)) {
            switch ($orientation) {
                case 3 :
                    $this->rotate($image, 180);
                    break;
                case 6 :
                    $this->rotate($image, 90);
                    break;
                case 8 :
                    $this->rotate($image, -90);
                    break;
            }

            $image->writeimage($filePath);
        }

        $image->destroy();
    }

    /**
     * Resize the image
     * @param string $filePath - source file path
     * @param int $width - resize width
     * @param int $height - resize height
     * @return $this
     */
    public function resize($filePath, $width, $height)
    {
        $image = new \Imagick($filePath);
        $image->resizeimage($width, $height, \Imagick::FILTER_LANCZOS, 1, true);
        $image->writeimage($filePath);
        $image->destroy();
        return $this;
    }

    /**
     * Rotate the image
     * @param \Imagick $imageContext
     * @param float $degrees
     * @return \Imagick
     */
    public function rotate(\Imagick $imageContext, $degrees)
    {
        $imageContext->rotateimage(new \ImagickPixel('transparent'), $degrees);

        return $imageContext;
    }

    /**
     * Crop and save image
     * @param string $filePath
     * @param string $size - crop coordinates separated by ';'
     * @return string - cropped and saved file name without path
     * @throws \yii\base\InvalidConfigException
     */
    public function crop($filePath, $size)
    {
        $this->checkPath($filePath);

        if (!$size) {
            throw new InvalidConfigException('Crop points are required.', 500);
        }

        $cropPoints = explode(';', $size);

        $image = new \Imagick($filePath);
        $width = $cropPoints[2] - $cropPoints[0];
        $image->cropimage($width, $width, $cropPoints[0], $cropPoints[1]);
        $image->writeimage($filePath);
        $image->destroy();

        $info = pathinfo($filePath);

        return "{$info['filename']}.{$info['extension']}";
    }

    /**
     * Crop and rotate hexagon and save result image in $destinationDirectory
     * @param string $filePath image for cropping
     * @param string $points rectangle coordinates (top left / bottom right)
     * @param float $rotateAngle the angle which image should be rotated (default `0.00`)
     * @param bool $abortOnError display errors or not
     * @return string saved cropped image's filename
     * @throws \yii\base\InvalidConfigException
     */
    public function cropHexagon($filePath, $points, $rotateAngle = 0.00, $abortOnError = true)
    {
        if ($abortOnError) {
            $this->checkPath($filePath);
        }

        if (!$points) {
            throw new InvalidConfigException('Crop points are required.', 500);
        }

        $cropPoints = explode(';', $points);
        $width = $cropPoints[2] - $cropPoints[0];
        $polygonDots = $this->calculateHexagon($width);
        $info = pathinfo($filePath);

        //fix image orientation
        $this->fixOrientation($filePath);

        //crop source image
        $this->crop($filePath, $points);

        //create mask
        $mask = new \Imagick;
        $mask->newimage($width, $width, new \ImagickPixel('transparent'), 'png');

        //draw polygon
        $polygon = new \ImagickDraw;
        $polygon->setFillColor(new \ImagickPixel('black'));
        $polygon->polygon($polygonDots);
        $mask->drawimage($polygon);
        $mask = $this->rotate($mask, $rotateAngle);

        $maskWidth = $mask->getimageheight();

        //merge cropped image with hex mask
        $image = new \Imagick;
        $image->readimage($filePath);
        $image->setImageFormat('png');
        $image->setImageVirtualPixelMethod(\Imagick::VIRTUALPIXELMETHOD_TRANSPARENT);
        $image->setImageMatte(true);
        $image->resizeimage($maskWidth, $maskWidth, \Imagick::FILTER_LANCZOS, 1, true);

        $compositionOffsetY = 0;
        if ($rotateAngle == 0) {
            $compositionOffsetY = - $image->getimageheight() / 12;
        }
        $image->compositeimage($mask, \Imagick::COMPOSITE_DSTIN, 0, $compositionOffsetY, \Imagick::CHANNEL_ALPHA);
        $image->trimimage(0);
        $image->writeimage($this->destinationDirectory . $info['filename'] . '.png');

        $image->destroy();
        $mask->destroy();

        unlink($filePath);

        return $info['filename'] . '.png';
    }

    private function calculateHexagon($width)
    {
        $polygon = [
            [
                'x' => 0.25 * $width, 'y' => 0.067  * $width
            ],
            [
                'x' => 0, 'y' => 0.5 * $width
            ],
            [
                'x' => 0.25 * $width, 'y' => 0.933 * $width
            ],
            [
                'x' => 0.75 * $width, 'y' => 0.933 * $width
            ],
            [
                'x' => $width, 'y' => 0.5 * $width
            ],
            [
                'x' => 0.75 * $width, 'y' => 0.067 * $width
            ]
        ];

        return $polygon;
    }

    public function checkPath($filePath)
    {
        if (!$filePath || !is_file($filePath)) {
            throw new InvalidConfigException('File path is invalid.', 500);
        }
    }
}