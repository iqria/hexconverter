<?php

namespace app\components;

use yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use Aws\Sqs\SqsClient;

/**
 * Class QueueManager - base logic to work with Amazon SQS
 * @package app\components
 * @author Stanislav Hudkov <stanislav.hudkov@iqria.com>
 */
class QueueManager extends Component
{
    /**
     * @var SqsClient $client
     */
    private $client;

    /**
     * @var string $queueUrl - the URL of remote queue or created in flow
     */
    public $queueUrl;

    /**
     * @var string $awsKey - AWS public API key
     */
    public $awsKey;

    /**
     * @var string $awsSecret - AWS secret API key
     */
    public $awsSecret;

    /**
     * @var string $awsRegion - AWS region
     * @see http://docs.aws.amazon.com/general/latest/gr/rande.html#sqs_region
     */
    public $awsRegion;

    public function init()
    {
        foreach (['awsKey', 'awsSecret', 'awsRegion'] as $attribute) {
            if ($this->$attribute === null) {
                throw new InvalidConfigException(strtr('"{class}::{attribute}" cannot be empty.', [
                    '{class}' => static::className(),
                    '{attribute}' => '$' . $attribute
                ]));
            }
        }

        $this->client = SqsClient::factory([
            'key'    => $this->awsKey,
            'secret' => $this->awsSecret,
            'region' => $this->awsRegion,
        ]);

        if (!$this->queueUrl) {
            $response = $this->client->createQueue([
                'QueueName'  => uniqid(),
                'Attributes' => [
                    'DelaySeconds'       => 5,
                    'MaximumMessageSize' => 4096,
                ],
            ]);

            if ($response && $response->get('QueueUrl')) {
                $this->queueUrl = $response->get('QueueUrl');
            } else {
                throw new InvalidConfigException('Can not configure "queueUrl" attribute', 500);
            }
        }

        parent::init();
    }

    /**
     * List of all queues
     * @return \Guzzle\Service\Resource\Model
     */
    public function listQueues()
    {
        return $this->client->listQueues();
    }

    /**
     * Add message to queue
     * @param mixed $data
     * @throws \yii\base\InvalidConfigException
     */
    public function queue($data)
    {
        if (!$data) {
            throw new InvalidConfigException('Invalid data passed', 500);
        }

        $this->client->sendMessage([
            'QueueUrl'    => $this->queueUrl,
            'MessageBody' => $data,
        ]);
    }

    /**
     * Delete message from queue
     * @param $message
     * @return boolean
     * @throws \yii\base\InvalidConfigException
     */
    public function dequeue($message)
    {
        if (!$message) {
            throw new InvalidConfigException('Invalid data passed', 500);
        }

        $this->client->deleteMessage([
            'QueueUrl'      => $this->queueUrl,
            'ReceiptHandle' => $message['ReceiptHandle'],
        ]);

        return true;
    }

    /**
     * Read messages from queue
     * @return \Guzzle\Service\Resource\Model|null
     */
    public function read()
    {
        $messages = $this->client->receiveMessage([
            'QueueUrl' => $this->queueUrl,
            'VisibilityTimeout' => 10,
            'WaitTimeSeconds' => 10,
            'MaxNumberOfMessages' => 10
        ]);

        if ($messages) {
            return $messages;
        }

        return null;
    }

    public function clearQueue()
    {
        $this->client->purgeQueue([
            'QueueUrl'    => $this->queueUrl,
        ]);
    }
}