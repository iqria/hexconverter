<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\forms\UploadForm;
use yii\web\HttpException;
use yii\web\Response;

class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return Yii::$app->view->renderFile('@app/views/layouts/main.php');
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Provides 2 methods of retrieving processed image (API call | Default view page)
     * @return array|string
     * @throws \yii\web\HttpException
     */
    public function actionViewFile() {
        Yii::$app->getResponse()->format = Response::FORMAT_JSON;
        $fileId = Yii::$app->request->get('fileId', false);

        if (!$fileId) {
            throw new HttpException(404, 'Please specify valid file id.');
        }

        $fileName = "{$fileId}.png";

        $isRemoteExists = Yii::$app->resourceManager->fileExists($fileName);
        $fileUrl = Yii::$app->resourceManager->getUrl($fileName);

        if ($isRemoteExists) {
            if (!Yii::$app->session->has('fileHistory')) {
                $fileHistory[] = $fileId;
            } else {
                $fileHistory = Yii::$app->session->get('fileHistory');
                $fileHistory[] = $fileName;
            }

            Yii::$app->session->set('fileHistory', $fileHistory);
        }

        if (strcmp(Yii::$app->request->getContentType(), 'application/json') === 0) {
            if ($isRemoteExists) {
                return ['fileUrl' => $fileUrl];
            } else {
                throw new HttpException(404, 'Requested file was not processed yet.');
            }
        } else {
            throw new HttpException(500, 'Please set request header Content-Type: application/json');
        }
    }
}
