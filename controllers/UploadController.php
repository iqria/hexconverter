<?php

namespace app\controllers;

use yii;
use yii\web\UploadedFile;
use yii\web\Response;
use yii\helpers\Json;

use app\models\forms\UploadForm;

class UploadController extends yii\web\Controller
{
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        return [
            'verbs' => [
                'class' => yii\filters\VerbFilter::className(),
                'actions' => [
                    'index'  => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $model = new UploadForm;

        $model->file = UploadedFile::getInstanceByName('file');

        $model->setAttributes(Yii::$app->request->post());

        if ($model->validate()) {
            $uploadedFile = $model->upload();

            if ($uploadedFile) {
                Yii::$app->queueManager->queue(Json::encode([
                    'file' => $uploadedFile,
                    'cropSize' => $model->cropSize,
                    'resize' => $model->resize,
                    'rotate' => $model->rotate,
                ]));
                $fName = pathinfo($uploadedFile, PATHINFO_FILENAME);

                return [
                    'success' => true,
                    'fileId' => $fName
                ];
            }
        }

        return [
            'success' => false,
            'error' => $model->getErrors()
        ];
    }

}