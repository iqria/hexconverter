Hexer
================================

Nowadays hexagon shape is a new trend in web design. That's why we've made this cool service which allows you to convert your image in the shape of hexagon. Be in trend with us!


DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      models/             contains model classes
      runtime/            contains files generated during runtime
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources
      widgets/            contains application widgets



REQUIREMENTS
------------

The minimum requirement by this application that your Web server supports PHP 5.4.0.


CONFIGURATION
-------------

Rename the file `config/params.php.example` to `config/params.php` and set required directories and Amazon credentials.
