<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
	<header>
		<h1 class="logo">hexer <a href="http://iqria.com" target="_blank"><span>by <span>IQ</span>RIA</span></a></h1>
	</header>
	<main class="container">
		<div class="row">
			<div class="col-lg-10 col-md-10 col-sm-10">
				<div class="drop-zone">
					<div class="text">Drop your photo here</div>
					<form class="file-form">
						<input type="file" name="file" class="file">
					</form>
				</div>
				<div class="wrapper">
					<img src="" alt="">
				</div>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-2 sidebar">
				<button type="button" class="btn btn-primary reset">New</button>
				<button type="button" class="btn btn-info rotate" disabled="disabled"><i class="glyphicon glyphicon-repeat"></i>Rotate</button>
				<button type="button" class="btn btn-success crop" disabled="disabled">Hex it!</button>
				<div class="result">
					<img src="" alt="">
					<pre></pre>
					<a href="" class="btn btn-success">Download</a>
				</div>
			</div>
		</div>
	</main>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
