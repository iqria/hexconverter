<?php

namespace app\models\forms;

use yii;
use yii\base\Model;
use yii\web\HttpException;

class UploadForm extends Model {

    /**
     * @var yii\web\UploadedFile|null file attribute
     */
    public $file;

    /**
     * @var string
     */
    public $cropSize;

    /**
     * @var float
     */
    public $rotate = 0.00;

    /**
     * @var string
     */
    public $resize;

    public function rules()
    {
        return [
            [['file', 'cropSize'], 'required', 'message' => '{attribute} can not be empty'],
            [['file'], 'file', 'maxSize' => 1024 * 1024 * 2, 'mimeTypes' => ['image/png', 'image/jpeg']],
            [['rotate'], 'number'],
            [['resize'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'file' => 'Image'
        ];
    }

    public function upload()
    {
        if (!is_null($this->file)) {
            try {
                $ext = pathinfo($this->file->name, PATHINFO_EXTENSION);
                $fileName = uniqid() . '.' . $ext;
                $filePath = Yii::$app->params['paths']['uploads'] . $fileName;

                if ($this->file->saveAs($filePath)) {
                    chmod($filePath, 0666);
                    return $fileName;
                } else {
                    throw new HttpException(500, 'Could not upload file');
                }
            } catch (HttpException $e) {
                exit($e->getMessage());
            }
        }

        return false;
    }
}